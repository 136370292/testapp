package com.example.deviceownerforbt;

import android.bluetooth.BluetoothAdapter;

public class BlueToothController {
    // 成员变量
    private BluetoothAdapter mAdapter;
    /**
     * 构造函数
     */
    public BlueToothController(){
        // 获取本地的蓝牙适配器
        mAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public boolean isSupportBlueTooth(){
        // 若支持蓝牙，则本地适配器不为null
        if(mAdapter != null){
            return true;
        }
        // 否则不支持
        else{
            return false;
        }
    }

    /**
     * 判断当前蓝牙状态
     * @return true为打开，false为关闭
     */
    public boolean getBlueToothStatus(){
        // 断言,为了避免mAdapter为null导致return出错
        assert (mAdapter != null);
        // 蓝牙状态
        return mAdapter.isEnabled();
    }


}
