package com.example.deviceownerforbt;

import android.Manifest;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.UserManager;
import android.util.Log;
import android.view.View;
import android.view.ViewStructure;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.app.ActivityCompat;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends Activity implements View.OnClickListener {

    private TextView textview;
    private Button mButton;
    private Button mButton2;
    private Button mButton3;
    private static final int REQ_PERMISSION_CODE = 1;
    // 蓝牙权限列表
    public ArrayList<String> requestList = new ArrayList<>();
    // 实例化蓝牙控制器
    public BlueToothController btController = new BlueToothController();
    // 弹窗
    private Toast mToast;

    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);

        textview = (TextView) findViewById(R.id.textView);
        ;
        mButton = (Button) findViewById(R.id.button);
        mButton2 = (Button) findViewById(R.id.button2);
        mButton3 = (Button) findViewById(R.id.button3);

        mButton.setOnClickListener(this);
        mButton2.setOnClickListener(this);
        mButton3.setOnClickListener(this);

    }

    public int ControlBluetoothDevice(int state) {

        DevicePolicyManager mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName testDeviceAdmin = new ComponentName(this, TestDeviceAdminReceiver.class);
        //boolean adminActive = mDPM.isAdminActive(testDeviceAdmin);
        int ret = 0;
        if (state == 0) {
            if (mDPM.isAdminActive(testDeviceAdmin)) {
                mDPM.clearUserRestriction(testDeviceAdmin, UserManager.DISALLOW_BLUETOOTH);
                ret = 1;
            } else {
                Log.d("MainActivity", "Device Admin Not Active: ");
            }
        } else {
            if (mDPM.isAdminActive(testDeviceAdmin)) {
                mDPM.addUserRestriction(testDeviceAdmin, UserManager.DISALLOW_BLUETOOTH);
                ret = 1;
            } else {
                Log.d("MainActivity", "Device Admin Not Active: ");
            }
        }
        return ret;
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.button) {
            UserManager mgr = (UserManager) getSystemService(USER_SERVICE);
            Bundle res = mgr.getUserRestrictions();
            for(String key:res.keySet()){
                Log.i("BUndle Content","Key="+key+" " + "Content="+res.get(key));
                if(key.equals("no_bluetooth")){
                    Log.d("蓝牙结果：","蓝牙被禁用");
                    textview.setText("蓝牙功能已禁用");
                    return;
                }
            }
            // 获取蓝牙权限
            getPermision();
            // 判断当前蓝牙状态
            boolean ret = btController.getBlueToothStatus();
            // 弹窗显示结果
            if(ret){
                textview.setText("当前蓝牙已打开");
            }else{
                textview.setText("当前蓝牙已关闭");
            }
        } else if (v.getId() == R.id.button2) {
            ControlBluetoothDevice(0);
            textview.setText("蓝牙功能已启用");
        } else if (v.getId() == R.id.button3) {
            ControlBluetoothDevice(1);
            textview.setText("蓝牙功能已禁用");
        }
    }


    /**
     * 动态申请权限
     */
    public void getPermision(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
            requestList.add(Manifest.permission.BLUETOOTH_SCAN);
            requestList.add(Manifest.permission.BLUETOOTH_ADVERTISE);
            requestList.add(Manifest.permission.BLUETOOTH_CONNECT);
            requestList.add(Manifest.permission.ACCESS_FINE_LOCATION);
            requestList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            requestList.add(Manifest.permission.BLUETOOTH);
        }
        if(requestList.size() != 0){
            ActivityCompat.requestPermissions(this, requestList.toArray(new String[0]), REQ_PERMISSION_CODE);
        }
    }


}
