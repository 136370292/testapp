package com.example.deviceownerforbt;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

public class TestDeviceAdminReceiver extends DeviceAdminReceiver{
    @Override
    public void onReceive(@NonNull Context context, @NonNull Intent intent) {
        super.onReceive(context, intent);
        String action = intent.getAction();
        Log.e("TestDeviceAdminReceiver","action="+action);
        //action=android.app.action.DEVICE_ADMIN_ENABLED
    }
}
